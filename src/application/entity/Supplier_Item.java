/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

/**
 *
 * @author DELL
 */
@Entity
@Table(name = "construction_supplierItem")

public class Supplier_Item implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SUPPLIER_ITEM_ID")
    private Integer supplierItemId;

    @Column(name = "SUPPLIER_ITEM_MANUFACTURER")
    private String supplierItemManufacturer;

    @Column(name = "SUPPLIER_ITEM_DESCRIPTION")
    private String supplierItemDescription;

    @Column(name = "SUPPLIER_ITEM_U_O_M")
    private String supplierItemUOM;

    @Column(name = "SUPPLIER_ITEM_S_K_U")
    private String supplierItemSKU;
    
    //One to many mapping 
    @Column(name = "SUPPLIER_ITEM_SUPPLIER")
    private String supplier;

    @Column(name = "CREATED_AT", columnDefinition = "DATETIME", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "UPDATED_AT", columnDefinition = "DATETIME", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @Column(name = "DELETED_AT", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;

    @Column(name = "STATUS", columnDefinition = "TINYINT(1)", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean status;
  
}

